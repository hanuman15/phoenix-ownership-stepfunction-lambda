package com.amazonaws.lambda.ownership.stepfunction.dto;

import org.json.JSONArray;

public class Response {
	 String path;
	 JSONArray source;
	 
	    public JSONArray getSource() {
		return source;
	}
	public void setSource(JSONArray source) {
		this.source = source;
	}
		public String getPath() {
		return path;
	}
	public void setPath(String path) {
		this.path = path;
	}

		@Override
	    public String toString() {
	        return "MyLambdaResponse [responseMessage=" + path ; 
	    }

}
