package com.amazonaws.lambda.ownership.stepfunction.dto;

import java.io.Serializable;

public class OwnershipDto implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 34658198454822838L;

	private Integer maxSubsidiarielevels;
	private Integer lowRange;
	private Integer highRange;
	private String identifier;
	private Integer noOfSubsidiaries;
	private String organisationName;
	private String juridiction;
	private Boolean isSubsidiariesRequired;
	private String startDate;
	private String endDate;
	private String source;
	private String path;
	private String url;
	

	public String getPath() {
		return path;
	}

	public void setPath(String path) {
		this.path = path;
	}

	public String getUrl() {
		return url;
	}

	public void setUrl(String url) {
		this.url = url;
	}

	public Integer getMaxSubsidiarielevels() {
		return maxSubsidiarielevels;
	}

	public void setMaxSubsidiarielevels(Integer maxSubsidiarielevels) {
		this.maxSubsidiarielevels = maxSubsidiarielevels;
	}

	public Integer getLowRange() {
		return lowRange;
	}

	public void setLowRange(Integer lowRange) {
		this.lowRange = lowRange;
	}

	public Integer getHighRange() {
		return highRange;
	}

	public void setHighRange(Integer highRange) {
		this.highRange = highRange;
	}

	public String getIdentifier() {
		return identifier;
	}

	public void setIdentifier(String identifier) {
		this.identifier = identifier;
	}

	public Integer getNoOfSubsidiaries() {
		return noOfSubsidiaries;
	}

	public void setNoOfSubsidiaries(Integer noOfSubsidiaries) {
		this.noOfSubsidiaries = noOfSubsidiaries;
	}

	public String getOrganisationName() {
		return organisationName;
	}

	public void setOrganisationName(String organisationName) {
		this.organisationName = organisationName;
	}

	public String getJuridiction() {
		return juridiction;
	}

	public void setJuridiction(String juridiction) {
		this.juridiction = juridiction;
	}

	public Boolean getIsSubsidiariesRequired() {
		return isSubsidiariesRequired;
	}

	public void setIsSubsidiariesRequired(Boolean isSubsidiariesRequired) {
		this.isSubsidiariesRequired = isSubsidiariesRequired;
	}

	public String getStartDate() {
		return startDate;
	}

	public void setStartDate(String startDate) {
		this.startDate = startDate;
	}

	public String getEndDate() {
		return endDate;
	}

	public void setEndDate(String endDate) {
		this.endDate = endDate;
	}

	public String getSource() {
		return source;
	}

	public void setSource(String source) {
		this.source = source;
	}

}
