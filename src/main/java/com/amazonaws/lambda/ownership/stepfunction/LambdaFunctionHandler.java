package com.amazonaws.lambda.ownership.stepfunction;

import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.util.ResourceBundle;
import java.util.UUID;
import java.util.concurrent.Future;

import javax.servlet.http.HttpServletRequest;

import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;

import com.amazonaws.ClientConfiguration;
import com.amazonaws.lambda.ownership.stepfunction.advancesearch.controller.AdvanceSearchController;
import com.amazonaws.lambda.ownership.stepfunction.dto.HierarchyDto;
import com.amazonaws.lambda.ownership.stepfunction.dto.OwnershipDto;
import com.amazonaws.lambda.ownership.stepfunction.dto.Response;
import com.amazonaws.regions.Regions;
import com.amazonaws.services.lambda.runtime.Context;
import com.amazonaws.services.lambda.runtime.RequestStreamHandler;
import com.amazonaws.services.stepfunctions.AWSStepFunctionsAsync;
import com.amazonaws.services.stepfunctions.AWSStepFunctionsAsyncClientBuilder;
import com.amazonaws.services.stepfunctions.model.StartExecutionRequest;
import com.amazonaws.services.stepfunctions.model.StartExecutionResult;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;

public class LambdaFunctionHandler implements RequestStreamHandler/* <InputStream, Response> */ {
	
ResourceBundle resource = ResourceBundle.getBundle("application");
	
	private String STEP_FUNCTION_ARN=resource.getString("step_function_arns");
	

	HttpServletRequest request;
	static AnnotationConfigApplicationContext ctx = new AnnotationConfigApplicationContext(
			LambdaFunctionHandler.class.getPackage().getName());

	public LambdaFunctionHandler() {
		ctx.getAutowireCapableBeanFactory().autowireBean(this);
	}

	@Autowired
	AdvanceSearchController adc;

	@Override
	public void handleRequest(/* OwnershipDto input, */InputStream inputStream, OutputStream output, Context context) {
		Response lambdaResponse = new Response();
		HierarchyDto jsonString = new HierarchyDto();

		OwnershipDto inputDto = new OwnershipDto();
		try {
			//
			JsonParser parser = new JsonParser();
			InputStreamReader reader = new InputStreamReader(inputStream);
			JsonElement element = parser.parse(reader);
			if (element.isJsonObject()) {
				JsonObject jsonObj = element.getAsJsonObject();
				System.out.println("Check ==" + jsonObj);
				if (jsonObj.has("body")) {
					JSONParser parser2 = new JSONParser();
					JSONObject json = (JSONObject) parser2.parse(jsonObj.get("body").getAsString());
					System.out.println("json >>> " + json);
					ObjectMapper mapper = new ObjectMapper();
					// Convert JSON to POJO
					// OwnershipDto ownerDto = mapper.readValue(json.toJSONString(),
					// OwnershipDto.class);
					inputDto = mapper.readValue(json.toJSONString(), OwnershipDto.class);
					if (inputDto.getIdentifier() != null) {
						System.out.println("ownershipDto.getIdentifier()>> " + inputDto.getIdentifier());

					}
				}
			}
			/*
			 * final ObjectMapper objectMapper = new ObjectMapper(); JsonNode jsonObj =
			 * objectMapper.readTree(inputStream); System.out.println("Check ==" +
			 * jsonObj.get("body"));
			 */

			String path = adc.ownershipStructure(jsonString, inputDto.getMaxSubsidiarielevels(), inputDto.getLowRange(),
					inputDto.getHighRange(), request, inputDto.getIdentifier(), inputDto.getNoOfSubsidiaries(),
					inputDto.getOrganisationName(), inputDto.getJuridiction(), inputDto.getIsSubsidiariesRequired(),
					inputDto.getStartDate(), inputDto.getEndDate(), inputDto.getSource());
			lambdaResponse.setPath(path);
			inputDto.setPath(path);
			//lambdaResponse.setSource(adc.getSource(inputDto.getUrl()));
			System.out.println("Path--> " + path + " -- " + inputDto.getIdentifier());
			org.json.JSONObject json = new org.json.JSONObject(inputDto);
			String name = UUID.randomUUID().toString();
			//String stateMachineArn = "arn:aws:states:eu-west-1:163124817352:stateMachine:phoenix-ownership-steps";
			String stateMachineArn=System.getenv("STEP_FUNCTION_ARNS");
			// unComment after testing inputStream
			StartExecutionRequest startExecutionRequest = new StartExecutionRequest();

			startExecutionRequest.setStateMachineArn(stateMachineArn);
			System.out.println("Starting state machine input " + stateMachineArn);
			startExecutionRequest.setInput(json.toString());
			startExecutionRequest.setName(name);
			System.out.println("Creating aws step Function client  " + name);

			AWSStepFunctionsAsync client = AWSStepFunctionsAsyncClientBuilder.standard()
					.withClientConfiguration(new ClientConfiguration()).withRegion(Regions.EU_WEST_1).build();

			System.out.println("Step function execution started " + client);
			Future<StartExecutionResult> ds = client.startExecutionAsync(startExecutionRequest);
			System.out.println("After start execution line " + ds.get().getSdkResponseMetadata());
			// org.json.JSONObject headerJson = new org.json.JSONObject();
			// headerJson.put("x-custom-header ", value)
			org.json.JSONObject lambdaresponseJson = new org.json.JSONObject(lambdaResponse);
			org.json.JSONObject responseJson = new org.json.JSONObject();
			responseJson.put("statusCode", 200);
			responseJson.put("body", lambdaresponseJson.toString());
			OutputStreamWriter writer = new OutputStreamWriter(output, "UTF-8");
			writer.write(responseJson.toString());
			writer.close();

		} catch (Exception e) {
			e.printStackTrace();
		}
		// return lambdaResponse;
	}

}